Title: repository/nicoo has been removed from ::unavailable-unofficial
Author: Niels Ole Salscheider <olesalscheider@exherbo.org>
Content-Type: text/plain
Posted: 2018-07-01
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: repository/nicoo

::nicoo[1] has been removed from ::unavailable-unofficial since it is
not maintained anymore.

If you use any packages from ::nicoo, you can resurrect them at any
time by following the instructions on our Contributing[2] doc and
maintain them in your own repository.

[1]: https://github.com/nbraud/nicoo_exherbo.git
[2]: https://exherbo.org/docs/contributing.html
